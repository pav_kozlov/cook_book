cook_book = [
    ['салат',
     [
         ['картофель', 100, 'гр.'],
         ['морковь', 50, 'гр.'],
         ['огурцы', 50, 'гр.'],
         ['горошек', 30, 'гр.'],
         ['майонез', 70, 'мл.'],
     ]
     ],
    ['пицца',
     [
         ['сыр', 50, 'гр.'],
         ['томаты', 50, 'гр.'],
         ['тесто', 100, 'гр.'],
         ['бекон', 30, 'гр.'],
         ['колбаса', 30, 'гр.'],
         ['грибы', 20, 'гр.'],
     ],
     ],
    ['фруктовый десерт',
     [
         ['хурма', 60, 'гр.'],
         ['киви', 60, 'гр.'],
         ['творог', 60, 'гр.'],
         ['сахар', 10, 'гр.'],
         ['мед', 50, 'мл.'],
     ]
     ]
]

dishes_list = []
for dishes in cook_book:
    dishes_list.append(dishes[0])
print("Имеются рецепты: ")
for dishes in dishes_list:
    print(' - ', dishes.capitalize())

selected_dish = input("\nЧто готовим? ").lower()
person = int(input("Сколько человек? "))

for dishes in cook_book:
    if selected_dish == dishes[0]:
        print('\n' + dishes[0].upper())
        for ingredients in dishes[1]:
            print(' - {} ({} грамм)'.format(ingredients[0], ingredients[1] * person))
    elif selected_dish not in dishes_list:
        print("\nТакого блюда нет в спике!")
        break
